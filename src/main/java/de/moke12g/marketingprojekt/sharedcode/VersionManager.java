package de.moke12g.marketingprojekt.sharedcode;

import de.creeperit.lmcprotocol.Protocol;

import java.util.ArrayList;

public class VersionManager {
    public static ArrayList<String> getVersionAsArrayList(String version) {
        ArrayList<String> strings = new ArrayList<String>();
        strings.add(version);
        strings.add(sharedmarketingcode.version);
        strings.add(Protocol.version);
        return strings;
    }

    public static String[] getVersion(String version) {
        return getVersionAsArrayList(version).toArray(new String[0]);
    }
}
