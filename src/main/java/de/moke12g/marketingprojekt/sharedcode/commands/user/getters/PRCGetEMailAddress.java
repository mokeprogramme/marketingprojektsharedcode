package de.moke12g.marketingprojekt.sharedcode.commands.user.getters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// The EMail Address cannot be changed

public class PRCGetEMailAddress implements prcCommand {
    public String getShort() {
        return "gea";
    }

    public String getLong() {
        return "getEMailAddress";
    }
}
