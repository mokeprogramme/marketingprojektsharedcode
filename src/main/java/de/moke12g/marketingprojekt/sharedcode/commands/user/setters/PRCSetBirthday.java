package de.moke12g.marketingprojekt.sharedcode.commands.user.setters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCSetBirthday implements prcCommand {
    public String getShort() {
        return "sbd";
    }

    public String getLong() {
        return "setBirthday";
    }
}
