package de.moke12g.marketingprojekt.sharedcode.commands.user.getters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCGetBirthday implements prcCommand {
    public String getShort() {
        return "gbd";
    }

    public String getLong() {
        return "getBirthDay";
    }
}
