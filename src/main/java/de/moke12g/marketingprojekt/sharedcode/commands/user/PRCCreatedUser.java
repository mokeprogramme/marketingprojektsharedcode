package de.moke12g.marketingprojekt.sharedcode.commands.user;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Gibt der Server nach erfolgreicher User erstellung zurück
// Das erste Argument gibt den UserTag, den der User bekommen hat zurück

public class PRCCreatedUser implements prcCommand {
    public String getShort() {
        return "cus";
    }

    public String getLong() {
        return "createdUserSuccessfully";
    }
}
