package de.moke12g.marketingprojekt.sharedcode.commands.session;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCLoginSuccessfully implements prcCommand {
    public String getShort() {
        return "lgs";
    }

    public String getLong() {
        return "loggedInSuccessfully";
    }
}
