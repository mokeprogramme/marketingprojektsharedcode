package de.moke12g.marketingprojekt.sharedcode.commands.user.getters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCGetLastName implements prcCommand {
    public String getShort() {
        return "gln";
    }

    public String getLong() {
        return "getLastName";
    }
}
