package de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Fordert vom anderem Teilnehmer die Version an

public class PRCGetVersion implements prcCommand {
    public String getShort() {
        return "gtv";
    }

    public String getLong() {
        return "getVersion";
    }
}
