package de.moke12g.marketingprojekt.sharedcode.commands.session.ping;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Wenn einen der Command Ping erreicht hat, wird mit Pong geantwortet

public class PRCPong implements prcCommand {
    public String getShort() {
        return "pog";
    }

    public String getLong() {
        return "pong";
    }
}