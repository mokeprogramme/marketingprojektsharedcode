package de.moke12g.marketingprojekt.sharedcode.commands.user.getters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// The Nickname cannot be changed

public class PRCGetNickname implements prcCommand {
    public String getShort() {
        return "gnn";
    }

    public String getLong() {
        return "getNickname";
    }
}
