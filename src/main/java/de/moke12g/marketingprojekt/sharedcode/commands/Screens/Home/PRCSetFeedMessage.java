package de.moke12g.marketingprojekt.sharedcode.commands.Screens.Home;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Gibt den Text zurück, der auf dieser Position angezeigt werden soll
// Der Rückgabewert false heißt NULL
// ARG 0 = String / "false"
// ARG 1 = Message ID

public class PRCSetFeedMessage implements prcCommand {
    public String getShort() {
        return "sfm";
    }

    public String getLong() {
        return "setFeedMessage";
    }
}
