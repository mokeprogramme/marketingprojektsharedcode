package de.moke12g.marketingprojekt.sharedcode.commands.session;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Dieser Command wird zum anmelden beim Server nach dem Sitzungsaufbau verschickt.
// Der Client sendet außerdem
// ARG0 = nickname
// ARG1 = usertag
// ARG2 = passwd <- Als Hash

public class PRCLogin implements prcCommand {
    public String getShort() {
        return "lgn";
    }

    public String getLong() {
        return "loginOnServer";
    }
}
