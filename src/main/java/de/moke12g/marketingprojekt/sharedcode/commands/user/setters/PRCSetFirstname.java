package de.moke12g.marketingprojekt.sharedcode.commands.user.setters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCSetFirstname implements prcCommand {
    public String getShort() {
        return "sfn";
    }

    public String getLong() {
        return "setFirstname";
    }
}