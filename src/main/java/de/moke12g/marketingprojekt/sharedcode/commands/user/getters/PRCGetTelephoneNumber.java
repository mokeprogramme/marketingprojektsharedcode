package de.moke12g.marketingprojekt.sharedcode.commands.user.getters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// The Telephone Number cannot be changed

public class PRCGetTelephoneNumber implements prcCommand {
    public String getShort() {
        return "gtn";
    }

    public String getLong() {
        return "getTelephoneNumber";
    }
}
