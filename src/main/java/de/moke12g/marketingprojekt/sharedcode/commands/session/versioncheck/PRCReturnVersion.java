package de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Gibt die geforderten Versionsdaten zurück

public class PRCReturnVersion implements prcCommand {
    public String getShort() {
        return "gvr";
    }

    public String getLong() {
        return "getVersion";
    }
}
