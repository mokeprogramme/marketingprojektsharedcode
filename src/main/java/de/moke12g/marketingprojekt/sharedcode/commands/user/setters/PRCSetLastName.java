package de.moke12g.marketingprojekt.sharedcode.commands.user.setters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCSetLastName implements prcCommand {
    public String getShort() {
        return "sln";
    }

    public String getLong() {
        return "setLastName";
    }
}
