package de.moke12g.marketingprojekt.sharedcode.commands.user;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Erstelle einen Leeren benutzer, welcher danach von der App mit Daten gefüttert wird
// Wichtig ist, dass beim erstellen des Nutzers der Nickname mitgesendet wird und das passwort als hash auch noch angehängt wird
// ARG 0 = nickname
// ARG 1 = passwordHash
// Der Server antwortet dann mit CUS = createdUserSuccessfully
// **Dieser Command meldet NICHT an! Er registriert nur den Benutzer!**

public class PRCCreateUser implements prcCommand {
    public String getShort() {
        return "cru";
    }

    public String getLong() {
        return "createUser";
    }
}
