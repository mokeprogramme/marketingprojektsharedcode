package de.moke12g.marketingprojekt.sharedcode.commands.Screens.Home;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Frage den Server nach, was in einer bestimmten Textbox / Nachricht (welche auf dem Home Menü angezeigt wird) steht
// ARG = number (die Nummer der Message)

public class PRCGetFeedMessage implements prcCommand {
    public String getShort() {
        return "gfm";
    }

    public String getLong() {
        return "getFeedMessage";
    }
}
