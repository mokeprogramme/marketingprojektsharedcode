package de.moke12g.marketingprojekt.sharedcode.commands.user.setters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCSetTelephoneNumber implements prcCommand {
    public String getShort() {
        return "stn";
    }

    public String getLong() {
        return "setTelephoneNumber";
    }
}
