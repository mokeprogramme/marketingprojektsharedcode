package de.moke12g.marketingprojekt.sharedcode.commands.session.ping;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

// Ping den Partner an
// Dieser wird mit Pong antworten

public class PRCPing implements prcCommand {
    public String getShort() {
        return "pig";
    }

    public String getLong() {
        return "ping";
    }
}
