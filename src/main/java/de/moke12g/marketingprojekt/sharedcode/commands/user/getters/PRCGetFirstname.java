package de.moke12g.marketingprojekt.sharedcode.commands.user.getters;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class PRCGetFirstname implements prcCommand {
    public String getShort() {
        return "gfn";
    }

    public String getLong() {
        return "getFirstname";
    }
}
