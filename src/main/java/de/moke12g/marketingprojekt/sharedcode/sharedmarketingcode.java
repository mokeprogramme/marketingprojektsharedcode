package de.moke12g.marketingprojekt.sharedcode;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;
import de.creeperit.lmcprotocol.Protocol;
import de.moke12g.marketingprojekt.sharedcode.commands.Screens.Home.PRCGetFeedMessage;
import de.moke12g.marketingprojekt.sharedcode.commands.Screens.Home.PRCSetFeedMessage;
import de.moke12g.marketingprojekt.sharedcode.commands.Screens.Home.clicker.PRCGetLinkedURL;
import de.moke12g.marketingprojekt.sharedcode.commands.Screens.Home.clicker.PRCSetLinkedURL;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLogin;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLoginFailed;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLoginSuccessfully;
import de.moke12g.marketingprojekt.sharedcode.commands.session.ping.PRCPing;
import de.moke12g.marketingprojekt.sharedcode.commands.session.ping.PRCPong;
import de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck.PRCGetVersion;
import de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck.PRCReturnVersion;
import de.moke12g.marketingprojekt.sharedcode.commands.user.PRCCreateUser;
import de.moke12g.marketingprojekt.sharedcode.commands.user.PRCCreatedUser;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.*;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.*;

import java.util.ArrayList;

public class sharedmarketingcode {

    public static String version = "05.04.2020";
    public static String serverHostname = "creeper-it.de";
    public static Integer serverPort = 42337;
    public static boolean debug = true;

    public static void setupProtocol() {

        // let the protocol init
        Protocol.init();

        // Account
        // Home
        //  clicker
        Protocol.prcCommandList.add(new PRCSetLinkedURL());
        Protocol.prcCommandList.add(new PRCGetLinkedURL());
        // Home
        Protocol.prcCommandList.add(new PRCGetFeedMessage());
        Protocol.prcCommandList.add(new PRCSetFeedMessage());
        // Search
        // Settings
        // Shopping

        // ping
        Protocol.prcCommandList.add(new PRCPing());
        Protocol.prcCommandList.add(new PRCPong());
        // versioncheck
        Protocol.prcCommandList.add(new PRCGetVersion());
        Protocol.prcCommandList.add(new PRCReturnVersion());
        // session
        Protocol.prcCommandList.add(new PRCLogin());
        Protocol.prcCommandList.add(new PRCLoginFailed());
        Protocol.prcCommandList.add(new PRCLoginSuccessfully());
        // getters
        Protocol.prcCommandList.add(new PRCGetBirthday());
        Protocol.prcCommandList.add(new PRCGetEMailAddress());
        Protocol.prcCommandList.add(new PRCGetFirstname());
        Protocol.prcCommandList.add(new PRCGetLastName());
        Protocol.prcCommandList.add(new PRCGetNickname());
        Protocol.prcCommandList.add(new PRCGetTelephoneNumber());
        // setters
        Protocol.prcCommandList.add(new PRCSetBirthday());
        Protocol.prcCommandList.add(new PRCSetEMailAddress());
        Protocol.prcCommandList.add(new PRCSetFirstname());
        Protocol.prcCommandList.add(new PRCSetLastName());
        Protocol.prcCommandList.add(new PRCSetNickname());
        Protocol.prcCommandList.add(new PRCSetTelephoneNumber());
        // user
        Protocol.prcCommandList.add(new PRCCreatedUser());
        Protocol.prcCommandList.add(new PRCCreateUser());

        if (debug) testForDuplicateSharedCode();
    }

    public static void testForDuplicateSharedCode() {
        System.out.println("Duplicates:");
        getDuplicates(convertCommandsToShort());
        System.out.println("-----------");
    }

    private static ArrayList<String> convertCommandsToShort() {
        ArrayList<String> shortCommands = new ArrayList<String>();
        for (prcCommand command : Protocol.prcCommandList) {
            shortCommands.add(command.getShort());
        }
        return shortCommands;
    }


    public static <T> ArrayList<T> getDuplicates(ArrayList<T> list) {
        // Create a new ArrayList
        ArrayList<T> newList = new ArrayList<T>();
        // Traverse through the first list
        for (T element : list) {
            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {
                newList.add(element);
            } else System.out.println(element);
        }
        // return the new list
        return newList;
    }

}
